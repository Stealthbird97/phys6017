#import pylab as py
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as py
from math import pi, cos

def f(x):
	"Sums Fourier series"
	sum=0.0
	term = 1.0
	lterm = 1.1
	n=2
	while abs(term - lterm) > 1e-6:
		lterm = term
		term=-cos(n*x)/((n-1)*(n+1))
		n=n+2.0
		sum=sum+term

	return sum + 0.5

#---------------------------------------------------------------------------

n=500
g=[f((4*pi*i)/n) for i in range(0,n)]

#graph(g)
py.plot(g)
py.show()