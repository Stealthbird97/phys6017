#import pylab as py
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as py
from math import pi, sin

def f(x):
	"Sums Fourier series"
	sum=0.0
	term=1.0
	lterm=1.1
	n=1
	while abs(term - lterm) > 1e-6: # Checks change in term between iterations
		lterm = term
		#print(lterm)
		term=sin(n*x)/n
		n=n+2.0
		sum=sum+term

	return sum

n=500
g=[f((4*pi*i)/n) for i in range(0,n)]

#graph(g)
py.plot(g)
py.show()