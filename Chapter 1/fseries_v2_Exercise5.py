import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as py
from math import pi, cos
def f(theta):
	"Sums Fourier series"
	sum=0.0
	term=1.0
	lterm = 1.1
	n=1.0
	cos_last = cos(theta)
	cos_now = cos_last
	c = 2 * cos(2 * theta)
	while abs(lterm - term) > 1e-5:  # Checks change in term between iterations
		lterm = term
		term = cos_now / (n * n)
		cos_last, cos_now = cos_now, c * cos_now - cos_last
		n=n+2.0
		sum=sum+term

	return sum

#---------------------------------------------------------------------------

n=500
g=[f((4*pi*i)/n) for i in range(0,n)]

#graph(g)
py.plot(g)
py.show()
