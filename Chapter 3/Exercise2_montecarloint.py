import numpy as np
from time import time

def f0(x):
    I = 1 / (1+x)**2
    return I

def f1(x):
    I = np.exp(-x**2)
    return I

def f2(x):
    I = np.exp(np.sin(x))
    return I

def f3(x):
    I = x**(0.5)
    return I

def montecarlo_int(N,a,b,f):
    sum = 0
    for i in range(0, N + 1):
        x = np.random.uniform(a, b)
        sum += f(x)
    result =(b-a)/N *sum
    return result

N = 1000000
expected=np.array([0.5,(np.pi)**0.5*np.erf(5),2*np.pi*np.i0(1),2/3])
print(expected[1])
start = time()
print(start)
#print("I0 = ", montecarlo_int(N,0,1,f0))
print("I1 = ", montecarlo_int(N,-5,5,f1))
#print("I2 = ", montecarlo_int(N,0,2*np.pi,f2))
#print("I3 = ", montecarlo_int(N,0,1,f3))
end = time()
print(end)
print("Time Elapsed = ", end - start)






