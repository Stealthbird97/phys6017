import numpy as np
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt
from time import time

def gaussint(a, b, delta):
    "Integrates f(x) between a and b with accuracy delta"
    n = 8
    f1 = I(n, a, b)
    f2 = -f1
    while abs(f2 - f1) > delta * abs(f1):
        if (n == 16384):
            print("Can't achieve accuracy requested")
            return 0
        f2 = f1
        n = 2 * n
        f1 = I(n, a, b)

    return f1


# ---------------------------------------------------------------------------*/

def I(n, a, b):
    "Divides range a to b into n panels"
    h = (b - a) / n
    sum = 0.0
    for i in range(0, n):
        sum += tenpoint(a + i * h, a + (i + 1) * h)
    return sum


def tenpoint(a, b):
    "Evaluates integral from a to b by 10 point Gauss formula"
    u = 0.5 * (a + b);
    v = 0.5 * (b - a)
    s = 0.2955242247 * (f(u + 0.1488743389 * v) + f(u - 0.1488743389 * v)) + \
        0.2692667193 * (f(u + 0.4333953941 * v) + f(u - 0.4333953941 * v)) + \
        0.2190863625 * (f(u + 0.6794095682 * v) + f(u - 0.6794095682 * v)) + \
        0.1494513491 * (f(u + 0.8650633666 * v) + f(u - 0.8650633666 * v)) + \
        0.0666713443 * (f(u + 0.9739065285 * v) + f(u - 0.9739065285 * v))

    return 0.5 * s * (b - a)


# ---------------------------------------------------------------------------*/

def f(x):
    L=1
    n=5
    "This defines the function to be integrated"
    return x * np.sin(glob_r * x) * (np.sin(L * x) / x) ** n

def P(r,L,n):
    global glob_r
    glob_r = r
    I = gaussint(0,5000, 1.0e-6)
    result = (2*r)/(np.pi*L**n)* I
    return result

n=3
L=1
r= np.linspace(0,n,100)
x = np.linspace(0,n,100)


for i in range(0,100):
    x[i] = P(r[i],L,n)
    #print(i, x[i])

plt.plot(r,x)
plt.show()
