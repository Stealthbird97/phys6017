import numpy as np
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt
from time import time

def montecarlo_int(N,a,b,f):
    sum = 0
    for i in range(0, N + 1):
        x = np.random.uniform(a, b)
        sum += f(x)
    result =(b-a)/N *sum
    return result

def f(x,r,L,n):
    result = x*np.sin(r*x)*(np.sin(L*x)/x)**n
    return result

N = 1000

start = time()
x = np.linspace(0.01,5,10000)
n=10
for r in range(0,n+1):
    plt.plot(x, f(x, r, 1, n), label="r={0}".format(r))
plt.legend()
plt.show()




end = time()
print("Time Elapsed = ", end - start)






