import numpy as np
from scipy import fftpack
from scipy.signal import windows
from scipy.fftpack import fft, fftfreq, fftshift
import matplotlib;

matplotlib.use('module://backend_interagg')  # To get sciview to work
import matplotlib.pyplot as plt
import copy
import time
import sys
from numba import jit
from bisect import bisect_left


# z[1] is x
# z[2] is y

@jit(nopython=True)
def derivs(n, t, z, omega):
    dz = np.zeros(n + 1)
    # dz=[0 for i in range(0,n+1)]
    dz[1] = P(t, omega) - z[1] * z[2] - z[1]
    dz[2] = z[1] * z[2] - r * z[2]
    return dz


@jit(nopython=True)
def P(tau, omega):
    return P0 + P1 * np.cos(omega * tauf * tau)


@jit(nopython=True)
def runkut(n, t, z, h, omega):
    z0 = z[:]
    k1 = derivs(n, t, z, omega)
    for i in range(1, n + 1):
        z[i] = z0[i] + 0.5 * h * k1[i]
    k2 = derivs(n, t + 0.5 * h, z, omega)
    for i in range(1, n + 1):
        z[i] = z0[i] + h * (0.2071067811 * k1[i] + 0.2928932188 * k2[i])
    k3 = derivs(n, t + 0.5 * h, z, omega)
    for i in range(1, n + 1):
        z[i] = z0[i] - h * (0.7071067811 * k2[i] - 1.7071067811 * k3[i])
    k4 = derivs(n, t + h, z, omega)
    for i in range(1, n + 1):
        a = k1[i] + 0.5857864376 * k2[i] + 3.4142135623 * k3[i] + k4[i]
        z[i] = z0[i] + 0.16666666667 * h * a
    t += h
    return (t, z)


c = 299792458
n_r = 1.77 #http://www.roditi.com/Laser/Ti_Sapphire.html
#wglth = 0.532e-6  # wavelength
sigma = 4.9e-23  # effective cross-sectional area

w0 = 1.5e-3
l = 65e-3  # length of gain medium
A = np.pi * w0**2  # area of beam at the waist
V = l * A  # approx. volume of beam in cavity
B = (sigma / V) * (c / n_r)  # calculation of B coeff.		### (c/n)*(sigma)/V

tauf = 3.3e-6
tauc = 150e-9
r = tauf/tauc #1500 #69690
P0 = 100 #199624 #13099624
P1 = 0.5 * P0
N = 1000000
tmax = 0.00001 #0.000020  # Time ms
taumax = tmax / tauf  # Time Dimensionless Units

@jit(nopython=True)
def laser(omega):
    tau = 0.0
    #omega = 1e7
    z = np.array([0.0, 0.0, 0.0001])  # BoundaryCond#
    # Calculate and print solution for t=0
    # to 1 using N steps
    x = np.zeros(N) #[0.0 for j in range(0, N)]
    y = np.zeros(N) #[0.0 for j in range(0, N)]
    ttau = np.zeros(N) # [0.0 for j in range(0, N)]
    n = np.zeros(N)# [0.0 for j in range(0, N)]
    q = np.zeros(N)# [0.0 for j in range(0, N)]
    t = np.zeros(N)# [0.0 for j in range(0, N)]
    for j in range(0, N):
        (tau, z) = runkut(2, tau, z, taumax / N, omega)
        #print(tau, z[1], z[2])
        ttau[j] = tau
        x[j] = z[1]
        y[j] = z[2]
        n[j] = x[j] / (B * tauf)
        q[j] = y[j] / (B * tauf)
        t[j] = ttau[j] * tauf * 1e3
    return x,y,n,q,t


def fft():
    T = tmax / N
    w = 1#windows.hamming(N//2)
    # f = np.linspace(0.0, 1/T, N/2)*2*np.pi
    qq = q[np.int(N / 2):]  # Selects the second half of the data where in steady state
    nn = n[np.int(N / 2):]  # Selects the second half of the data where in steady state
    f = fftpack.fftfreq(len(qq), T) * 2 * np.pi  # Calculates the bins freqs in rads/s
    f2 = f[1:len(f) // 2]  # Takes Positive Freqs Only
    qf = fftpack.fft(qq*w)  # Generate FFT
    qf2 = qf[1:len(qf) // 2]  # Takes Posiive Freqs Only
    nf = fftpack.fft(nn*w)  # Generate FFT
    nf2 = nf[1:len(nf) // 2]  # Take Positive Freqs Only
    return f2,qf2,nf2

responseres = 22
#omegaarray = np.linspace(4e7,5e7,100)
omegaarray = np.linspace(0.5e7,1e7,responseres)
#omegaarray = np.array([1e7])
response = np.zeros(responseres) #[0 for i in range(0,101)]

for i in range(0,responseres):
    start = time.time()
    x, y, n, q, t = laser(omegaarray[i])
    f2, qf2, nf2 = fft()
    response[i]=abs(nf2[np.argmax(nf2)])
    stop = time.time()
    print("time=", stop - start)
    #plt.plot(t,x,label="x (n)")
    #plt.plot(t,y, label="y (q)")
    #plt.plot(t, n, label="Inversion")
    #plt.plot(t, q, label="Photon Number")
    #plt.xlabel("Time (ms)")
    #plt.legend()
    #plt.show()

    file = omegaarray[i]

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rcParams.update({'font.size': 18})

    fig1, ax1 = plt.subplots()
    ax1.plot(t, n, 'b-', label="$n$", linewidth=0.5)
    ax1.set_xlabel('Time (ms)')
    ax1.set_ylabel('Population Inversion $(n)$', color='b')
    ax1.tick_params('y', colors='b')
    ax1.yaxis.set_major_locator(plt.MaxNLocator(7))
    ax1.legend(bbox_to_anchor=(0.70, 0.78), loc=2, borderaxespad=0.)

    ax2 = ax1.twinx()
    ax2.plot(t, q, 'r-', label="$q$", linewidth=0.5)
    ax2.set_ylabel('Photon Number $(q)$', color='r')
    ax2.tick_params('y', colors='r')
    ax2.yaxis.set_major_locator(plt.MaxNLocator(7))
    ax2.legend(bbox_to_anchor=(0.70, 0.95), loc=2, borderaxespad=0.)
    fig1.tight_layout()
    fig1.savefig("relaxation oscillations Ti P0_nomod.pdf", bbox_inches='tight')
    plt.show()

    fig3, ax4 = plt.subplots()
    ax4.set_xlim([0.0,8e7])
    ax4.set_xlim([0.5e6, 3.1e7])
    ax4.set_ylim([0.0, 60e18])
    ax4.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    ax4.set_ylabel('Amplitude')
    ax4.set_xlabel('Frequency (rad/s)')
    ax4.plot(f2, abs(nf2), 'black', linewidth=0.5)
    fig3.tight_layout()
    fig3.savefig(str(file) + '_fft.pdf', bbox_inches='tight')
    plt.show()
    """
    """


fig3, ax2 = plt.subplots()
ax2.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
ax2.set_ylabel('Amplitude')
ax2.set_xlabel('Frequency (rad/s)')
ax2.plot(omegaarray,response, 'black', linewidth=0.5)
fig3.tight_layout()
fig3.savefig("Response.pdf", bbox_inches='tight')
plt.show()

"""
plt.plot(omegaarray,response, 'black', linewidth=0.5)
plt.ylabel('Amplitude')
plt.xlabel("Frequency (rad/s)")
plt.savefig('Ti.pdf', dpi=300)
plt.show()
"""