import numpy as np
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt
import copy
import time
import sys
from numba import jit

#@jit(nopython=True)
def derivs(n,x,y):
    dy=np.zeros(n+1)
    #dy=[0 for i in range(0,n+1)]
    dy[1]=y[2]
    dy[2]=-y[1]
    return dy

#@jit(nopython=True)
def runkut(n,x,y,h):
    y0 = y[:]
    k1 = derivs(n, x, y)
    for i in range(1, n + 1):
        y[i] = y0[i] + 0.5*h*k1[i]
    k2 = derivs(n, x + 0.5*h, y)
    for i in range(1, n + 1):
        y[i] = y0[i] + h*(0.2071067811*k1[i]+0.2928932188*k2[i])
    k3 = derivs(n, x + 0.5*h, y)
    for i in range(1, n + 1):
        y[i] = y0[i] - h*(0.7071067811*k2[i]-1.7071067811*k3[i])
    k4 = derivs(n, x + h, y)
    for i in range(1, n + 1):
        a = k1[i] + 0.5857864376*k2[i] + 3.4142135623*k3[i] + k4[i]
        y[i] = y0[i] + 0.16666666667*h*a
    x += h
    return (x,y)

N=100
x=0.0
y=np.array([0.0,0.0,1.0])  # BoundaryCond#
# Calculate and print solution for x=0
# to 1 using N steps
start = time.time()
for j in range(0,N):
    (x,y)=runkut(2,x,y,1.0/N)
    print(x, y[1], y[2], np.sin(x), np.cos(x))
stop = time.time()
print(stop-start)