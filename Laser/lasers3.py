import numpy as np
from scipy import fftpack
from scipy.fftpack import fft, fftfreq, fftshift
import matplotlib;

matplotlib.use('module://backend_interagg')  # To get sciview to work
import matplotlib.pyplot as plt
import copy
import time
import sys
from numba import jit
from bisect import bisect_left


# z[1] is x
# z[2] is y

def derivs(n, t, z, omega):
    dz = np.zeros(n + 1)
    # dz=[0 for i in range(0,n+1)]
    dz[1] = P(t, omega) - z[1] * z[2] - z[1]
    dz[2] = z[1] * z[2] - r * z[2]
    return dz


def P(tau, omega):
    return P0 + P1 * np.cos(omega * tauf * tau)


def runkut(n, t, z, h, omega):
    z0 = z[:]
    k1 = derivs(n, t, z, omega)
    for i in range(1, n + 1):
        z[i] = z0[i] + 0.5 * h * k1[i]
    k2 = derivs(n, t + 0.5 * h, z, omega)
    for i in range(1, n + 1):
        z[i] = z0[i] + h * (0.2071067811 * k1[i] + 0.2928932188 * k2[i])
    k3 = derivs(n, t + 0.5 * h, z, omega)
    for i in range(1, n + 1):
        z[i] = z0[i] - h * (0.7071067811 * k2[i] - 1.7071067811 * k3[i])
    k4 = derivs(n, t + h, z, omega)
    for i in range(1, n + 1):
        a = k1[i] + 0.5857864376 * k2[i] + 3.4142135623 * k3[i] + k4[i]
        z[i] = z0[i] + 0.16666666667 * h * a
    t += h
    return (t, z)


c = 3e8
n_r = 1.82
wglth = 1.064e-6  # wavelength
sigma = 3e-23  # effective cross-sectional area

l = 0.1  # cavity length
w0 = np.sqrt(wglth * (l / np.pi))  # beam waist size
w_end = np.sqrt(2) * w0
lg = 0.005  # length of gain medium
A = np.pi * (w0 * w0)  # area of beam at the waist
V = l * A  # approx. volume of beam in cavity
B = (sigma / V) * (c / n_r)  # calculation of B coeff.		### (c/n)*(sigma)/V
Vg = lg * A  # approx. volume of beam within the gain medium

omegaarray = np.linspace(1e7, 20e7, 100)
response = [0 for i in range(0, 101)]

tauf = 230e-6
tauc = 150e-6
r = 69690
P0 = 13099624
P1 = 0.1 * P0
N = 100000
tmax = 0.000020  # Time ms
taumax = tmax / tauf  # Time Dimensionless Units

def laser(omega):
    tau = 0.0
    #omega = 1e7
    z = np.array([0.0, 0.0, 0.0001])  # BoundaryCond#
    # Calculate and print solution for t=0
    # to 1 using N steps
    x = [0.0 for j in range(0, N)]
    y = [0.0 for j in range(0, N)]
    ttau = [0.0 for j in range(0, N)]
    n = [0.0 for j in range(0, N)]
    q = [0.0 for j in range(0, N)]
    t = [0.0 for j in range(0, N)]
    for j in range(0, N):
        (tau, z) = runkut(2, tau, z, taumax / N, omega)
        # print(tau, z[1], z[2])
        ttau[j] = tau
        x[j] = z[1]
        y[j] = z[2]
        n[j] = x[j] / (B * tauf)
        q[j] = y[j] / (B * tauf)
        t[j] = ttau[j] * tauf * 1e3
    return x,y,n,q,t


def fft():
    T = tmax / N
    # f = np.linspace(0.0, 1/T, N/2)*2*np.pi
    qq = q[np.int(N / 2):]  # Selects the second half of the data where in steady state
    nn = n[np.int(N / 2):]  # Selects the second half of the data where in steady state
    f = fftpack.fftfreq(len(qq), T) * 2 * np.pi  # Calculates the bins freqs in rads/s
    f2 = f[1:len(f) // 2]  # Takes Positive Freqs Only
    qf = fftpack.fft(qq)  # Generate FFT
    qf2 = qf[1:len(qf) // 2]  # Takes Posiive Freqs Only
    nf = fftpack.fft(nn)  # Generate FFT
    nf2 = nf[1:len(nf) // 2]  # Take Positive Freqs Only
    return f2,qf2,nf2


omegaarray = np.linspace(1e7,20e7,100)
response = [0 for i in range(0,101)]

for i in range(0,101):
    start = time.time()
    x, y, n, q, t = laser(omegaarray[i])
    f2, qf2, nf2 = fft()
    stop = time.time()
    print(stop - start)
    print("time=", stop - start)
    # plt.plot(t,x,label="x (n)")
    # plt.plot(t,y, label="y (q)")
    plt.plot(t, n, label="Inversion")
    plt.plot(t, q, label="Photon Number")
    plt.xlabel("Time (ms)")
    plt.legend()
    plt.show()

    fig3, ax4 = plt.subplots()
    # ax4.set_xlim([0.0,8e6*2*np.pi])
    ax4.set_xlim([0.0, 20e7])
    ax4.set_ylim([0.0, 1.25e17])
    ax4.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    ax4.set_ylabel('Amplitude')
    ax4.set_xlabel('Frequency (rad/s)')
    ax4.plot(f2, abs(nf2), 'black', linewidth=0.5)
    fig3.tight_layout()
    fig3.savefig("fft n.pdf", bbox_inches='tight')

    plt.show()
    print(np.argmax(nf2))
    print(abs(nf2[np.argmax(nf2)]))