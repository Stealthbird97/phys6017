import numpy as np
import matplotlib.pyplot as plt
import copy
import time
import sys
from numba import jit

@jit(nopython=True)
def choosetype(): # Selects the Vehicle Type based on vehicle ratio set in #Type Ratio.
    type = 0
    random = np.random.uniform(0, 1)
    if random >= typeratio[0]:
        type = 0
    if typeratio[1] > random > typeratio[0]:
        type = 1
    if random >= typeratio[1]:
        type = 2
    return type

class Vehicle: # A vehicle
    def __init__(self, type, tlength, n):
        self.vid = n
        self.v = int(np.random.uniform(0.5,1) * vmax[type]) #Initial Velocity
        self.a = np.random.uniform(0,1) # Aggression
        self.s = np.random.uniform(0,1) # Slowness
        self.type = type # Type of Vehicle
        self.length = tlength # Length of Vehicle
    def setv(self,v): # Changes the Velocity
        print("Velocity=", self.v)
        self.v = v
        print("Vehicle Slows to Velocity=", self.v)
    def accelerate(self):
        print("Velocity=", self.v)
        if self.v < vmax[self.type]:
            self.v +=1
            print("Vehicle Accelerates to Velocity=", self.v)

class LongVehicle():
    def __init__(self, type, section,tlength,n):
        self.vid = n
        self.type = type # Type of Vehicle
        self.section = section # The section of the vehicle
        self.length = tlength

class Road:
    def __init__(self, n, length, lanes):
        self.map = np.full([length,lanes], None)
        for i in range(0, n):
            loop = 1
            type = choosetype()
            while loop == 1:
                lane = np.random.randint(0, lanes)
                if type != 0 and lanes >=2:
                    lane = np.random.randint(0, 2) # Lorries and Long Lorries can only use lanes 1 and 2 by law.
                position = np.random.randint(2, length) # Length of Road is shorter to allow for longer vehicles.
                if type == 0:
                    if self.map[position, lane] == None:
                        self.map[position, lane] = Vehicle(type, vlength[0], i)
                        loop = 0
                if type == 1:
                    if  self.map[position, lane] == None and self.map[position-1, lane] == None:
                        self.map[position, lane] = Vehicle(type, vlength[1],i)
                        self.map[position - 1, lane] = LongVehicle(type, 1, vlength[1],i)
                        loop = 0
                if type == 2:
                    if  self.map[position, lane] == None and self.map[position-1, lane] == None and self.map[position-2, lane] == None:
                        self.map[position, lane] = Vehicle(type, vlength[2],i)
                        self.map[position - 1, lane] = LongVehicle(type, 1, vlength[2],i)
                        self.map[position - 2, lane] = LongVehicle(type, 2, vlength[2],i)
                        loop = 0
    def destroyvehicle(self,i,j): # Destroys vehicle
        self.map[i,j] = None
        #print(self.map[i,j])

    def movevehicle(self,i,j,v): # moves the vehicle.
        print("Current Pos=", i)
        print("Velocity=",v)
        #if self.map[i+v,j] != None:
        #    print("ERROR: Collision. Tried to move to {0} but is not empty".format(i+v))
        #    sys.exit(1)
        self.map[i+v,j] = copy.deepcopy(self.map[i,j])
        print(self.map[i+v,j])
        print("New Pos=", i+v)

    def switchlane(self,i,j,k):
        self.map[i, j+k] = copy.deepcopy(self.map[i, j])

    def lookahead(self,i,j,v): # Checks is path is clear and if not finds the distance to the next vehicle.
        clearpath=1
        sep = 0
        for x in range(i+1,i+v+1):
            if self.map[x,j] != None:
                if type(self.map[x, j]) is Vehicle:
                    print("Collision with Vehicle ID=", self.map[x, j].vid)
                else:
                    print("Collision with Vehicle Section ID=", self.map[x, j].vid)
                clearpath = 0
                sep = x-i
                print("Proposed new Position=", i+v)
                print("Seperation=", sep)
                break
        return clearpath, sep

    def count(self,i,v):
        for x in range(0,len(checkpoints)):
            print(x)
            if i+v >=  checkpoints[x] > i:
                counts[x,t]+=1

def destory(road, vehicle,i,j): # Destorys an entire vehicle based on type
    print("Destroying Vehicle")
    for l in range(0,vehicle.length):
        road.destroyvehicle(i-l, j)

def move(road,vehicle, i,j): # Moves Entire Vehicle based on type
    clear,seperation = road.lookahead(i, j, vehicle.v)
    if clear == 0:
        vehicle.setv(seperation-1)
    if np.random.uniform(0, 1) <= unexpected and vehicle.v !=0:
        print("Vehicle Slowed Unexpectedly")
        vehicle.setv(vehicle.v - 1)
    for l in range(0,vehicle.length):
        road.movevehicle(i-l, j, vehicle.v)
        road.destroyvehicle(i-l, j)

def progress(road): # Processes the simulation by one unit time.
    map = road.map
    for i in range(rlength-1,-1,-1):
        for j in range(rlanes-1,-1,-1):
            if type(map[i,j]) is Vehicle:
                vehicle = map[i,j]
                print("Vehicle ID=", vehicle.vid, vehicle.type)
                print("Vehicle is at {0}, in lane {1}".format(i,j))
                vehicle.accelerate()
                road.count(i, vehicle.v)
                if i+vehicle.v >= rlength:
                    destory(road,vehicle,i,j)
                else:
                    move(road,vehicle,i,j)
            """
            elif type(map[i, j]) is LongVehicle:
                print("Long Vehicle ID=", vehicle.vid)
                print("Vehicle is at {0}, in lane {1}".format(i, j))
                print("Type i,j:",type(map[i, j]))
                if type(map[i+1, j]) is not Vehicle:
                    print("Type i+l,j:", type(map[i + 1, j]))
                    print("Detached")
                    return road
                    sys.exit(1)
            """
    return road

def display(map):
    r = np.where(map[:])[1] # Gets Lane index of vehicle
    x = np.where(map[:])[0] # Gets position index of vehicle
    theta = 2*np.pi * x/rlength # Gets position on loop
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='polar')
    c = ax.scatter(theta, r, c='red')
    ax.set_yticks(np.arange(0, rlanes))
    ax.xaxis.set_major_locator(plt.MultipleLocator(2 * np.pi * 50 / rlength))
    ax.set_rorigin(-2)
    plt.show()

def displayxy(map):
    x = np.where(map[:])[1] # Gets Lane index of vehicle
    y = np.where(map[:])[0] # Gets position index of vehicle
    fig = plt.figure()
    ax = fig.add_subplot(111)
    c = ax.scatter(x, y, c='blue', s=0.1)
    ax.set_yticks(np.arange(0, rlanes))
    ax.set_yticks(np.arange(0, rlength))
    ax.yaxis.set_major_locator(plt.MultipleLocator(100))
    #ax.set_xlim([0, rlanes])
    ax.set_ylim([0, rlength])
    plt.show()

n = 500 # Number of Vehicles
vmax = np.array([120,90,90]) # Maximum Speed
rlength = 1000 # Road Length
rlanes = 2 # Road Lanes
unexpected = 0.0 # Probability of distraction
vlength = np.array([1,2,3]) # Vehicle Lengths
typeratio = np.array([0.5,0.3,0.20]) # Vehicle Distributions

if typeratio[0]+typeratio[1]+typeratio[2]!=1:
    print("Vehicle Ratios Do not Sum to 1")
    sys.exit(1)
typeratio = np.array([typeratio[0],typeratio[0]+typeratio[1],typeratio[0]+typeratio[1]+typeratio[2]])


tmax = 20
cpsep = 5
checkpoints = np.array([i*rlength/cpsep for i in range(1,cpsep+1)]) # Position of Checkpoints along road.
counts = np.zeros([len(checkpoints),tmax])

road = Road(n,rlength,rlanes)
displayxy(road.map)

for t in range(0,tmax):
    start = time.time()
    print("t=", t)
    displayxy(road.map)
    progress(road)
    print(counts[:,t])
    plt.show()
    end = time.time()
    print(end - start)


"""
road1=copy.deepcopy(road)
progress(road1)
displayxy(road1.map)
road2=copy.deepcopy(road1)
progress(road2)
displayxy(road2.map)
road3=copy.deepcopy(road2)
progress(road3)
displayxy(road3.map)
"""