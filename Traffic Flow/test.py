def __init__(self, n, length, lanes):
    self.map = np.full([length, lanes], None)
    for i in range(0, n):
        type = choosetype()
        print(n,vlength[type])
        canfit = 0
        while canfit == 0:
            if type != 0:
                lane = np.random.randint(0, 2)  # Lorries and Long Lorries can only use lanes 1 and 2 by law.
            else:
                lane = np.random.randint(0, lanes)
            position = np.random.randint(2, length)  # Length of Road is shorter to allow for longer vehicles.
            canfit = 1
            for k in range(position, position - vlength[type], -1):
                if self.map[k, lane] != None:
                    canfit = 0
        self.map[position, lane] = Vehicle(type, vlength[type], i)
        print("Vehicle Created")
        for l in range(1, vlength[type]-1):
            print("Vehicle Section Created")
            self.map[position - 1, lane] = VehicleSec(type, vlength[type], i)