import numpy as np
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt
import copy
import time
import sys
from numba import jit
import csv

@jit(nopython=True)
def choosetype(): # Selects the Vehicle Type based on vehicle ratio set in #Type Ratio.
    type = 0
    random = np.random.uniform(0, 1)
    if random >= typeratio[0]:
        type = 0
    if typeratio[1] > random > typeratio[0]:
        type = 1
    if typeratio[2] > random > typeratio[1]:
        type = 2
    if typeratio[3] > random > typeratio[2]:
        type = 3
    if random >= typeratio[4]:
        type = 4
    return type

class Vehicle: # A vehicle
    def __init__(self, type, tlength, n):
        self.vid = n
        self.v = 0 #int(np.random.uniform(0.8,1) * vmax[type])  #Initial Velocity
        self.type = type # Type of Vehicle
        self.length = tlength # Length of Vehicle
        self.minsep = minsep
    def setv(self,v): # Changes the Velocity
        self.v = v
    def setsep(self): # Changes the Minimum Seperation
        self.minsep = minsep + int(0.5*self.v)
    def accelerate(self): # Accelerates the Vehicle if below speed limit.
        if self.v < vmax[self.type]:
            self.v += acc[self.type]

class VehicleSec():
    def __init__(self, type,tlength,n):
        self.vid = n
        self.type = type # Type of Vehicle
        self.length = tlength

class Road:
    def __init__(self, n, length, lanes):
        global glob_vid
        self.map = np.full([length+longestvehicle+minsep+vmax[np.argmax(vmax)]+1, lanes], None) # Road length = Active Length + Initialisation Space + 1 section to catch movement check errors
        for i in range(0, n):
            self.createvehicle(i,length,lanes,1)
            glob_vid +=1
        #print("Road Initialised")

    def createvehicle(self,i,length,lanes,init=0):
        global glob_sec
        type = choosetype()
        canfit = 0
        timeout = 100
        while canfit == 0 and timeout !=0:
            timeout-=1
            if timeout==0:
                print("Timed out")
                break
            if vlength[type]>5 and lanes > 2:
                lane = np.random.randint(0, lanes-1)  # Lorries and Long Lorries can not use outside lane.
            else:
                lane = np.random.randint(0, lanes)
            if init == 1:
                position = np.random.randint(longestvehicle, length+longestvehicle)  # Length of Road is shorter to allow for longer vehicles.
            else:
                position = longestvehicle-1
            canfit = 1
            for k in range(position, position - vlength[type], -1):
                if self.map[k, lane] != None:
                    canfit = 0
        self.map[position, lane] = Vehicle(type, vlength[type], i)
        glob_sec+=1
        for l in range(1, vlength[type]):
            self.map[position - l, lane] = VehicleSec(type, vlength[type], i)
            glob_sec += 1

    def destroyvehicle(self,i,j): # Destroys vehicle
        self.map[i,j] = None

    def danger(self,i,j,k,l):
        checkdanger = 0
        for z in range(0, k):  # Checks if vehicle can safely move
            if self.map[i - z, j + l] != None:
                checkdanger += 1
        return checkdanger

    def getvehicleahead(self,i,j):
        k = 1
        try:
            while type(road.map[i + k, j]) != Vehicle:
                k += 1
            nextv = road.map[i + k, j]
        except:
            nextv = road.map[i, j]
        return nextv

    def switchlane(self,i,j,k):
        self.map[i, j+k] = copy.deepcopy(self.map[i, j])
        self.map[i, j] = None

    def moveone(self,i,j):
        self.map[i+1, j] = copy.deepcopy(self.map[i, j])
        self.map[i, j] = None

    def count(self,i,v):
        for x in range(0,len(checkpoints)):
            if i+v >=  checkpoints[x] > i:
                counts[x,t]+=1

    def countone(self,i):
        for x in range(0,len(checkpoints)):
            if i+1 >=  checkpoints[x] > i:
                counts[x,t]+=1

def destory(road, vehicle,i,j): # Destorys an entire vehicle based on type
    for l in range(0,vehicle.length):
        road.destroyvehicle(i-l, j)

def move(road,vehicle, i,j): # Moves Entire Vehicle based on type
    if vehicle.v > 1:
        if np.random.uniform(0, 1) <= freedistraction:
                vehicle.setv(int(vehicle.v * (1-slow)))
    else:
        if np.random.uniform(0, 1) <= jamdistraction:
            vehicle.setv(0)
    v = vehicle.v
    while v >0:   # While the vehicle still has velocity to move
        if j != 0 and np.random.uniform(0,1)<=ltrans:   # Check if the vehicle is in the left most lane. If it is not, then it can try to move left.
            danger = road.danger(i,j,vehicle.length,-1)
            if danger == 0:  # If safe to move left.
                worth = 0
                for lead in range(1,vehicle.minsep+1):
                    if road.map[i+lead,j-1] != None:
                        worth += 1
                if worth == 0: # Is it worth moving left?
                    for l in range(0, vehicle.length): # Move vehicle left
                        road.switchlane(i - l, j, -1)
                    j -= 1
        worth = 0
        for lead in range(1, vehicle.minsep + 1):
            if road.map[i + lead + 1, j] != None:
                worth += 1
        if worth==0: # Check if can move forward.
            v -= 1
            for l in range(0, vehicle.length): # Move vehicle forward.
                road.moveone(i-l,j)
            road.countone(i)
            i += 1
            for l in range(0, vehicle.length):
                if road.map[i - l, j] == None:
                    print("i={0}, l={1}, i-l={2}".format(i,l,i-l))
                    print("Vehicle Seperated")
                    global broke
                    broke = 1
                    #sys.exit(1)
        else: # If can't move forward in current lane. Overtake?
            if j <= rlanes - 2 and np.random.uniform(0,1)<=rtrans:  # Checks vehicles is not in outside lane and if vehicle is likely to overtake.
                danger = road.danger(i,j,vehicle.length,1)  # Checks if vehicle can move right to overtake.
                if j == rlanes-2 and vlength[vehicle.type]>5: # Long vehicles can not use outside lane
                    danger = 1
                if danger ==0:
                    worth =0
                    for lead in range(1, vehicle.minsep + 1):
                        if road.map[i + lead, j + 1] != None:
                            worth += 1
                    if worth ==0:  # Checks if overtaking is worth it..
                        for l in range(0, vehicle.length):
                            road.switchlane(i-l, j, 1)
                            road.moveone(i-l,j+1)
                        v -= 1
                        i += 1
                        j += 1
                    else: # If overtake is not worth it, sets speed to that of vehicle ahead and stops further movement in this time frame.
                        nextv = road.getvehicleahead(i,j)
                        vehicle.setv(nextv.v)
                        v = 0
                else:  # If dangerous to overtake, sets speed to that of vehicle ahead and stops further movement in this time frame.
                    nextv = road.getvehicleahead(i, j)
                    vehicle.setv(nextv.v)
                    v = 0
            else: #If in outside lane, sets speed to that of vehicle ahead and stops further movement in this time frame.
                nextv = road.getvehicleahead(i, j)
                vehicle.setv(nextv.v)
                v = 0

def progress(road): # Processes the simulation by one unit time.
    map = road.map
    for i in range(rlength+longestvehicle-1,-1,-1):
        for j in range(rlanes-1,-1,-1):
            if type(map[i,j]) is Vehicle:
                vehicle = map[i,j]
                vehicle.accelerate()
                vehicle.setsep()
                if i+vehicle.v >= rlength+longestvehicle:
                    road.count(i, vehicle.v)
                    destory(road,vehicle,i,j)
                else:
                    move(road,vehicle,i,j)

    return road

def inflow(road,influx,length,lanes):
    global glob_vid
    for i in range(0,influx):
        road.createvehicle(glob_vid,length,lanes)
        glob_vid += 1
    return road

def display(map):
    r = np.where(map[:])[1] # Gets Lane index of vehicle
    x = np.where(map[:])[0] # Gets position index of vehicle
    theta = 2*np.pi * x/rlength # Gets position on loop
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='polar')
    c = ax.scatter(theta, r, c='red')
    ax.set_yticks(np.arange(0, rlanes))
    ax.xaxis.set_major_locator(plt.MultipleLocator(2 * np.pi * 50 / rlength))
    ax.set_rorigin(-2)
    plt.show()

def displayxy(map):
    x = np.where(map[:])[1] # Gets Lane index of vehicle
    y = np.where(map[:])[0] # Gets position index of vehicle
    fig = plt.figure()
    ax = fig.add_subplot(111)
    c = ax.scatter(x, y, c='blue', s=0.1)
    ax.set_yticks(np.arange(0, rlanes))
    ax.set_yticks(np.arange(0, rlength))
    ax.yaxis.set_major_locator(plt.MultipleLocator(100))
    ax.set_ylim([0, rlength+longestvehicle+minsep+1])
    plt.show()

def density():
    sum = 0
    lasttraffic = -1
    for i in range(tmax - 1, -1, -1):
        sum += counts[4][i]
        if lasttraffic == -1 and counts[4][i] != 0:
            lasttraffic = i
    density = sum / lasttraffic
    return density, sum, lasttraffic

broke=0
vmax = np.array([32,32,27,27,27]) # Maximum Speed (ms-1)
acc = np.array([2,2,1,1,1]) # Acceleration (ms-2)
rlength = 1000 # Active Road Length (m)
rlanes = 1 # Road Lanes
freedistraction = 0.75 # Probability of distraction
jamdistraction = 0.1
slow = 0.1
ltrans=0.5
rtrans=1
vlength = np.array([2,5,15,12,17]) # Vehicle Lengths
longestvehicle = vlength[np.argmax(vlength)] # Length of the longest vehicle
minsep = 1
typeratio = np.array([0.006205042,0.930707666,0.002470676,0.028732746,0.03188387]) # Vehicle Distributions
influx = 0 # Rate of Flow into road
sum = 0
for i in range(0,len(typeratio)):
    sum += typeratio[i]
if sum !=1:
    print("Vehicle Ratios Do not Sum to 1")
    sys.exit(1)
else:
    for i in range(1,len(typeratio)):
        typeratio[i]+=typeratio[i-1]

n = 50 # Number of Vehicles
glob_vid = 0  # Keeps track of vehicle id's
glob_sec = 0
tmax = 1  # Simulation Time
cpsep = 5  # Number of Checkpoitns
checkpoints = np.array(
    [i * rlength / cpsep + longestvehicle for i in range(1, cpsep + 1)])  # Position of Checkpoints along road.
counts = np.zeros([len(checkpoints), tmax])  # Flow Through checkpoints
counts2 = 0
road = Road(n, rlength, rlanes)
displayxy(road.map)
for t in range(0, tmax):
    start = time.time()
    # print("t=", t)
    displayxy(road.map)
    for i in range(rlength + longestvehicle - 1, -1, -1):
        for j in range(rlanes - 1, -1, -1):
            da = np.array([[road.map[i,j],t]])
            data = np.append(data,da,axis=0)
    progress(road)
    inflow(road, influx, rlength, rlanes)
    # print(counts[:,t])
    if road.map[900 + longestvehicle, 0] != None:
        counts2 += 1
    plt.show()
    end = time.time()
    print(end - start)