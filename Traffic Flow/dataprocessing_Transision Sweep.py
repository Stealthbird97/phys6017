import numpy as np
from scipy.optimize import curve_fit

def log(x,A,B,C,D):
    return A * np.log(B*x+C)+D

def quadasy(x,A,B,D,E):
    return (A*x**2+B*x)/(D*x**2+E*x+1)

def linear(x,A,B):
    return A*x+B

def linfit(xdata,ydata):
    popt, pcov = curve_fit(linear, xdata, ydata, bounds=(([-np.inf,-np.inf], [np.inf,np.inf])), maxfev=100000)
    return popt

def log_fit(xdata,ydata):
    popt, pcov = curve_fit(log, xdata, ydata, bounds=(([-1.,0.,0.,-10.], [20., 20.,20.,20.])), maxfev=10000)
    return popt

def quad_asy(xdata,ydata):
    popt, pcov = curve_fit(quadasy, xdata, ydata, bounds=(([-np.inf,-np.inf,-np.inf,-np.inf], [np.inf, np.inf,np.inf,np.inf])), maxfev=100000)
    return popt

file = '3lanes_transprob.csv'
#file = '3lanes_always_overtake_lanehog.csv'
#file = '3lanes_overtake_always_return.csv'

data4 = np.genfromtxt(file, delimiter=',')

length = np.shape(data4)

uniq, cnts = np.unique(data4[:,5], return_counts=1) # Gets unique Values, and number of ocurances of unique value

sum = np.zeros([np.size(uniq)])
for i in range(0,length[0]): # Each row in array
    section = data4[i,5] # Gets the value for sections
    b=np.argwhere(uniq==section) # Finds argument for this number of sections
    sum[b]+=data4[i,2]/cnts[b] # Sum of Averages
flowrate=sum # The Average Flow Rate

sdsum = np.zeros([np.size(uniq)])
for i in range(0,length[0]): # For each row in array
    section = data4[i,5] # Gets number of sections
    b=np.argwhere(uniq==section) # Findd argument for this numebr of sections
    sdsum[b]+=(data4[i,2]/cnts[b]-flowrate[b])**2 # Sum of Std Deviation
std = np.sqrt(sdsum/cnts)



import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

x=uniq
plt.scatter(x,flowrate,  c="blue", s=0.5, label="Simulation Data")

x2 = uniq[15:]
flow2 = flowrate[15:]
fit3 = linfit(x2,flow2)

plt.plot(x, linear(x,*fit3), c="orange", label='Fit: a=%5.3f, b=%5.3f' % tuple(fit3))
#fit = log_fit(x,flowrate)
#fit3 = quad_asy(x,flowrate)
#plt.plot(x, log(x,*fit))
#plt.plot(x, quadasy(x,*fit3))
#plt.errorbar(x, flowrate, std, linestyle='None', marker='^')
#plt.plot(x, quadasy(x,*fit3), c="orange", label='Fit: a=%5.3f, b=%5.3f, c=%5.3f, d=%5.3f' % tuple(fit3))
plt.xlabel(r'Lane Change Probability')
plt.ylabel(r' Flow Rate [Vehicles $s^{-1}$]')
plt.yticks(np.arange(1.2, 2.3, step=0.2))
plt.legend()
plt.savefig(file + '.png', dpi=300)

plt.show()
"""
uniq2, cnts2 = np.unique(data4[:,0], return_counts=1)
sum2 = np.zeros([np.size(uniq2)])

for i in range(0,length[0]):
    section = data4[i,0]
    b=np.argwhere(uniq2==section)
    sum2[b]+=data4[i,2]

x2 = uniq2
flowrate2=sum2/cnts2

fit2 = log_fit(x2,flowrate2)
fit4 = quad_asy(x2,flowrate2)
plt.plot(uniq2,flowrate2)
plt.plot(x2, log(x2,*fit2))
plt.plot(x2, quadasy(x2,*fit4))
plt.show()
"""