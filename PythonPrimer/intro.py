"""

This is a comment block

"""

#mydict = {"tom": 345, "jo": 678}
#mylist = ['apples', 'oranges', 'pears', 'mangos', 5.0]
#print(mydict)
#print(mylist[1])

"""
Flow Control

a = 5.2
b = 5.1
if a==b:
    print("of course!")
elif a>b:
    print("First is greater!")
else:
    #print(a,b, "not equal!")
    print("THe quantities %.2f and %.1f are not equal."%(a,b))
    
"""

mylist = ['apples', 'oranges', 'pears', 'mangos', 5.0]
for s in mylist:
    print(s)

x=0
while x <= 10:
    print(x)
    x+=2
    if x == 4:
        break

import os
print(os.environ['PATH'])