"""
Plotting
"""
import numpy
import matplotlib.pyplot as plt
x = numpy.linspace(-numpy.pi, numpy.pi, 256)
y = numpy.cos(x)
plt.plot(x,y)
plt.show()