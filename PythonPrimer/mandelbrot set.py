import numpy
import matplotlib.pyplot as plt

def calc_mandelbrot(Nmax, N, nx, ny):
    x = numpy.linspace(-2,1,nx)
    y = numpy.linspace(-1.5,1.5,ny)
    xx, yy = numpy.meshgrid(x,y)
    c = xx + 1j * yy
    z = numpy.zeros((nx,ny), dtype=numpy.complex)
    amps  = numpy.zeros((nx,ny), dtype=numpy.double)
    mask = numpy.ones((nx,ny), dtype=numpy.bool)
    for i in range(Nmax):
        z[mask] = z[mask]**2 + c[mask]
        mask[:] = numpy.abs(z) < N
        amps[mask] = float(i)
    return amps

mbrot = calc_mandelbrot(50,2,500,500)
plt.imshow(mbrot)
plt.show()