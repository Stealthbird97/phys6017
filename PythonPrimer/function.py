"""
Use of Functions

"""
#from math import pi
import math
import numpy
pi = math.pi

#def testing():
 #   print("myfunction")

def area(r):
    a = pi*r**2
    return a

def double_it(v):
    return 2*v

def double_it_area2():
    r = 2.0
    r2 = double_it(r)
    a = area(r)
    a2 = area(r2)
    return (a,a2)

def double_it_area(r,f1,f2):
    r2 = f1(r)
    a2 = f2(r2)
    return a2

import numpy

radii = [1,2,3,4,5]

#print(radii + radii)

radii = numpy.array(radii, dtype=numpy.double) # convert to numpy array

#print(radii + radii)
#print(radii**2*pi)

#print(len(radii))
"""
areas=[]
for x in radii:
    areas.append(double_it_area(x,double_it,area))
print(areas)

print(area(1.0))
"""

def npy_area(ar):
    ar[:] = ar*ar*pi

print(radii)
npy_area(radii)
print(radii)