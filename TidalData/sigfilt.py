from physics import *
import pylab

def amplitude(x):
	"Robust estimate of the amplitude of an approximate  sine wave"
	sumx=0
	sumsq=0
	for i in range(0,N): sumx+=x[i]
	for i in range(0,N): sumsq+=(x[i]-sumx/N)*(x[i]-sumx/N)
	
	return sqrt(2*sumsq/N) + sumx/N

#-----------------------------------------------------------------------------

def cyclicfilter(x, w0, w1, w2, w3):
	"Filters x with weights w3,w2,w1,w0,w1,w2,w3"
	z=zeros(N,float)
	m3=N-3; m2=N-2; m1=N-1; p1=1; p2=2; p3=3
	for i in range(0,N):
		z[i]=w0*x[i]+w1*(x[p1]+x[m1])+w2*(x[p2]+x[m2])+w3*(x[p3]+x[m3])
		m1=(m1+1)%N; m2=(m2+1)%N; m3=(m3+1)%N
		p1=(p1+1)%N; p2=(p2+1)%N; p3=(p3+1)%N
	return z
#----------------------------------------------------------------------------

def rfilter(x, a0, a1, a2, b1, b2):
	"Filters x with recusive filter"
	y=zeros(N,float)
	x0=x[0]; x1=x[1]
	y[0]=a0*x0
	y[1]=a0*x1+a1*x0-b1*y[0]
	for i in range(2,N):
		x2=x1; x1=x0; x0=x[i]
		y[i]=a0*x0+a1*x1+a2*x2-b1*y[i-1]-b2*y[i-2]
	return y
#----------------------------------------------------------------------------

N=128
N2=N/2
x=zeros(N,float)
g=zeros(N2,float)
pylab.clf()

for k in range(0,N2):                                # loop over frequency
	for n in range(0,N): x[n]=cos((2*pi*n*k)/N)      # sinusoidal input
	y=cyclicfilter(x, 0.5, 0.25, 0.0, 0.0)     
	g[k]=amplitude(y)

pylab.plot(g)
pylab.show()

#---------------------------------------------------------------------------
#
#    This is provided so you can copy it and not make a mistake typing it!
#    y = rfilter(x, 0.281235, 0.312673, 0.281235, -0.668737, 0.523879)
#
#----------------------------------------------------------------------------
