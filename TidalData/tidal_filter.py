import numpy as np
import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt
import copy
import time
import sys
from numba import jit

ocean = np.genfromtxt('ocean.dat', dtype=float) # Imports Dataset
ocean_r = np.flip(ocean) # Duplicates and Flips Data
ocean_ext = np.append(ocean_r,ocean) # Duplicates Flipped Data and appends original dataset to end
ocean_ext = np.append(ocean_ext,ocean_r) # Appends flipped data to extended dataset

N=1024
def rfilter(x, a0, a1, a2, a3, a4, b1, b2, b3, b4):
    y=np.zeros(N,float)
    x0=x[0]; x1=x[1];x2=x[2];x3=x[3];x4=x[4]
    y[0]= a0*x0
    y[1]=a0*x1+a1*x0-b1*y[0]
    y[2]=a0*x2+a1*x1+a2*x0-b1*y[1]-b2*y[0]
    y[3]=a0*x3+a1*x2+a3*x1-b1*y[2]-b2*y[1]-b3*y[0]
    y[4]=a0*x4+a1*x3+a3*x2+a4*x1-b1*y[3]-b2*y[2]-b3*y[1]-b4*y[0]
    for i in range(5,N-5):
        x4=x3; x3=x2;x2=x1;x1=x0;x0=x[i]
        y[i]=a0*x0+a1*x1+a2*x2+a3*x3+a4*x4-b1*y[i-1]-b2*y[i-2]-b3*y[i-3]-b4*y[i-4]
    return y


filtered = rfilter(ocean, 0, -0.1173629592912053, 0.1743779371672954, -0.1173629592846880, 0.0302062466510130, -3.9133921631643922, 5.7604234417533462, -3.7796979160704920, 0.9327311493782693)
filtered2 = rfilter(filtered, 0, -0.1173629592912053, 0.1743779371672954, -0.1173629592846880, 0.0302062466510130, -3.9133921631643922, 5.7604234417533462, -3.7796979160704920, 0.9327311493782693)


x = np.arange(1,1025)
plt.plot(x,ocean)
plt.plot(x,filtered)
plt.xticks(np.arange(0, 1024, step=52))
plt.show()