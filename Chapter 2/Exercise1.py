import matplotlib; matplotlib.use('module://backend_interagg') # To get sciview to work
import matplotlib.pyplot as plt
import time
import hashlib

def randomsec(n,x=time.time()):
    sec = []
    a = 78789
    N = 2**15 - 1
    for i in range(0,n):
        x = a*x%N
        sec.append(x)
    return sec

sec = randomsec(1000,10)
secx = sec[0:][::2]
secy = sec[1:][::2]
print(sec)
plt.scatter(secx,secy, marker='+')
plt.show()


