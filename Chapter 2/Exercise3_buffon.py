from random import uniform
from math import sin, pi
n=100000
for k in range(1,6):
	c=0
	for i in range(0,n):
		x=uniform(0,1)
		y=uniform(0,1)
		phi=pi*uniform(0,1)
		theta=phi+pi/2
		if theta > pi:
			theta-=pi
		sx=0.5*sin(phi)
		sy=0.5*sin(theta)
		if ((x<sx) or (1-x<sx) or (y<sy) or (1-y<sy)):
			c+=1
		
	print(k, float(c)/n, 2/pi)
