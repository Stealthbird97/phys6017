from random import uniform
from matplotlib import pylab
from math import *
import numpy as np
def distance(n):
    """Computes Distance moved after n random steps"""
    x=y=z=0
    for i in range(0,n):
        s = randomsphere(3)[0]
        x+=s[0]
        y+=s[1]
        z+=s[2]
        #c=2.0*uniform(0,1)-1.0
        #s=sqrt(1.0-c**2)
        #phi = 2.0*pi*uniform(0,1)
        #x+=s*cos(phi)
        #y+=s*sin(phi)
        #z+=c
    return sqrt(x**2+y**2+z**2)

def randomsphere(n):
    diag = 100
    while diag > 1:
        i = np.random.random(n) * 2 - 1.0
        diag = sqrt(np.dot(i, i))
        i = i / diag
    return i, diag

def approximation(r,L,n,dr,W):
    A = sqrt(6/pi)*(3*r**2/L**3)*n**(-3/2)*exp(-3*(r/L)**2/2*n)
    return W*A*dr

nbins = 200
jmax = nbins-1
n=200 # Number of Steps per Walk
nwalk = 500 # Number of Walks
deltaR=(1.1*n)/jmax
m=[0 for i in range(0, jmax)]
for k in range(0, nwalk):
    R=distance(n)
    j=int(floor(R/deltaR+0.5))
    if j<jmax:
        m[j]+=1



print(approximation(500,1,n,1,nwalk))

pylab.bar(range(len(m)),m,width=1.)
#pylab.bar(range(len(m)),approximation([0 for i in range(0, jmax)],1,n,deltaR,nwalk))
pylab.show()

