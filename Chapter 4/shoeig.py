from numpy import *
from numpy.linalg import eigh as _eigh, solve as _solve
import pylab

def mateig(A):
    (d,A)=_eigh(A)
    return (A,d)

N=1000                             # matrix size
NSQ=N*N

dx=1/sqrt(N)                            # spacing of x points
centre=0.5*(N-1)*dx
off_diagonal=-0.5/(dx*dx)               # off diagonal elements of matrix
A=zeros((N,N), float)                   # set whole matrix to zero

for i in range(0,N-1):
	A[i, i+1]=off_diagonal              # set off diagonal elements
	A[i+1, i]=off_diagonal

for i in range(0,N):                    # set diagonal elements
	x=i*dx
	A[i,i]=1/(dx*dx)+0.5*(x-centre)*(x-centre)

(A,val) = mateig(A)                     # compute eigenvalues and vectors 

print(val)
psi1= A[:,0]
psi2= A[:,1]
psi3=-A[:,2]
pylab.plot(psi3)
pylab.plot(psi2)
pylab.plot(psi1)
pylab.show()
